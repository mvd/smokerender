Matt DelBrocco
EECS 466 Project
12/17/12

Modeling and Rendering Smoke

Obviously, I am not the first person to try to do this. I closely followed Jos Stam's paper [1], so the results are very similar. I modified the OpenGL implementation a bit and added some additional functionality (mostly in the form of keyboard input). Basically, I took it, learned from it, and expanded on it a little - in my opinion, this made the project an effective learning experience.

[1] Jos Stam, "Stable Fluids", SIGGRAPH 1999, 121-128 (1999)


