void performTimestep();
void densStep ( float * x, float * x0, float * u, float * v, float diff, float dt );
void velStep ( float * u, float * v, float * u0, float * v0, float visc, float dt );