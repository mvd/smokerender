/*
Matt DelBrocco 
12/17/12
Final Project

Implemented with guidance from Jos Stam's papers, "Stable Fluids" and "Real-Time Fluid Dynamics for Games" as well as a third paper by Fedkiw et. al.

[1] Jos Stam, "Stable Fluids", SIGGRAPH 1999, 121-128 (1999)
[2] Jos Stam, "Real-Time Fluid Dynamics for Games", Proceedings of the Game Developer Conference, March 2003
[3] Fedkiw, R., Stam, J. and Jensen, H.W., "Visual Simulation of Smoke", SIGGRAPH 2001, 23-30 (2001)

I've mainly taken the approach outlined in [2].

The basic idea is to divide the space up into a grid of cells (whether 2D or 3D, the concept applies).
Density and velocity are defined at the center of each cell. The meat of the program is in updating these values, which are stored in respective arrays.
Then a simple density draw function colors the pixels based on density values.
*/

// Allow use of M_PI constant
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <stdlib.h>
#include "glut.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#include "main.h"
#include <vector>

#include <iostream>
#include <fstream>

#define N 128						// number of cells to have in each dimension of our grid (e.g. N=64 results in a 64x64 grid)
#define size (N+2) * (N+2)
#define IX(i,j) ((i)+(N+2)*(j))		// a simple way to index our "2D" array, which is in fact stored as a single-dimension array.
#define SWAP(x0,x) {float *tmp=x0;x0=x;x=tmp;}

using namespace std;

// Global variables
int windowWidth, windowHeight;    // Window dimensions

float dt, diff, visc, source, force;
float u[size], v[size], u_prev[size], v_prev[size];		// velocity arrays
float dens[size], dens_prev[size];							// density arrays
int omx, omy, mx, my;
bool mouseState[3];
bool bound; // use this to determine whether smoke can leave the viewing area or not
bool bg_blue;		// false sets bg to black, true sets it to blue

// useful for resetting the simulation without restarting the program
void clearArrays ()
{
	for (int i=0 ; i<size ; i++ ) {
		u[i] = v[i] = u_prev[i] = v_prev[i] = dens[i] = dens_prev[i] = 0.0f;
	}
}

void densityDraw()
{
	int i, j;
	float x, y, d1, d2, d3, d4;
	float h = 1.0f/N;

	glBegin(GL_QUADS);
	for(i = 0; i <= N; i++)
	{
		x = (i-0.5f)*h;
		for(j = 0; j <= N; j++)
		{
			y = (j-0.5f)*h;

			// get color for each corner of the quad we're about to draw
			d1 = dens[IX(i,j)];
			d2 = dens[IX(i+1,j)];
			d3 = dens[IX(i+1,j+1)];
			d4 = dens[IX(i,j+1)];

			if(bg_blue)
			{
				glColor3f(d1, d1, 1.0f); 
				glVertex2f(x, y);

				glColor3f(d2, d2, 1.0f); 
				glVertex2f(x+h, y);

				glColor3f(d3, d3, 1.0f); 
				glVertex2f(x+h, y+h);

				glColor3f(d4, d4, 1.0f); 
				glVertex2f(x, y+h);
			}
			else
			{
				glColor3f(d1, d1, d1); 
				glVertex2f(x, y);

				glColor3f(d2, d2, d2); 
				glVertex2f(x+h, y);

				glColor3f(d3, d3, d3); 
				glVertex2f(x+h, y+h);

				glColor3f(d4, d4, d4); 
				glVertex2f(x, y+h);
			}
		}
	}
	glEnd();
}

// The display function. It is called whenever the window needs
// redrawing (ie: overlapping window moves, resize, maximize)
// You should redraw your polygons here
void	display(void)
{
	glViewport ( 0, 0, windowWidth, windowHeight);
	glMatrixMode ( GL_PROJECTION );
	glLoadIdentity ();
	gluOrtho2D ( 0.0, 1.0, 0.0, 1.0 );
	// Clear the background
	glClearColor ( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	densityDraw();

    glutSwapBuffers();  // swaps buffers of the current window if double buffered
}

// This function is called whenever the window is resized. 
// Parameters are the new dimentions of the window
void	resize(int x,int y)
{
    glViewport(0,0,x,y);
    windowWidth = x;
    windowHeight = y;
    
    //printf("Resized to %d %d\n",x,y);
}


// This function is called whenever the mouse is pressed or released
// button is a number 0 to 2 designating the button
// state is 1 for release 0 for press event
// x and y are the location of the mouse (in window-relative coordinates)
void	mouseButton(int button,int state,int x,int y)
{
	omx = mx = x;
	omx = my = y;

	mouseState[button] = state == GLUT_DOWN;  // mouseState is true if the mouse is pressed, false otherwise
}

//This function is called whenever the mouse is moved with a mouse button held down.
// x and y are the location of the mouse (in window-relative coordinates)
void	mouseMotion(int x, int y)
{
	mx = x;
	my = y;
}

// This function is called whenever there is a keyboard input
// key is the ASCII value of the key pressed
// x and y are the location of the mouse
void	keyboard(unsigned char key, int x, int y)
{
	int i = (int)(N/2);
	int j = (int)(N/2);
	int max = N-1;
	int min = 2;
    switch(key) {
    case 'q':                           /* Quit */
		exit(1);
		break;
	case 'g':		// add density at the center of the screen
		dens[IX(i,j)] = source;
		break;
	case 'b':		// add density at the bottom center of the screen
		j = min;
		dens[IX(i,j)] = source;
		break;
	case 't':		// add density at the top center of the screen
		j = max;
		dens[IX(i,j)] = source;
		break;
	case 'f':		// add density at the middle left of the screen
		i = min;
		dens[IX(i,j)] = source;
		break;
	case 'v':		// add density at the bottom left corner of the screen
		i = min;
		j = min;
		dens[IX(i,j)] = source;
		break;
	case 'r':		// add density at the top left corner of the screen
		i = min;
		j = max;
		dens[IX(i,j)] = source;
		break;
	case 'h':		// add density at the middle right of the screen
		i = max;
		dens[IX(i,j)] = source;
		break;
	case 'n':		// add density at the bottom right corner of the screen
		i = max;
		j = min;
		dens[IX(i,j)] = source;
		break;
	case 'y':		// add density at the top right corner of the screen
		i = max;
		j = max;
		dens[IX(i,j)] = source;
		break;
	case 'i':	// force up & right from center
		u[IX(i,j)] = force;
		v[IX(i,j)] = force;
		break;
	case 'u':	// force up & left from center
		u[IX(i,j)] = -force;
		v[IX(i,j)] = force;
		break;
	case 'j':	// force down & left from center
		u[IX(i,j)] = -force;
		v[IX(i,j)] = -force;
		break;
	case 'k':	// force down & right from center
		u[IX(i,j)] = force;
		v[IX(i,j)] = -force;
		break;
	case 'p':		// force up & right from bottom left corner
		i = min;
		j = min;
		u[IX(i,j)] = force;
		v[IX(i,j)] = force;
		break;
	case 'o':		// force up & left from bottom right corner
		i = max;
		j = min;
		u[IX(i,j)] = -force;
		v[IX(i,j)] = force;
		break;
	case 'l':		// force down & left from top right corner
		i = max;
		j = max;
		u[IX(i,j)] = -force;
		v[IX(i,j)] = -force;
		break;
	case ';':		// force down & right from top left corner
		i = min;
		j = max;
		u[IX(i,j)] = force;
		v[IX(i,j)] = -force;
		break;
	case 'a':		// force straight up from the bottom center
		j = min;
		v[IX(i,j)] = force;
		break;
	case 'z':
		bound = !bound;
		if(bound)
			cout << "Bounds are now in effect." << endl;
		else
			cout << "The simulation is now unbounded." << endl;
		clearArrays();
		break;
	case 'x':
		bg_blue = !bg_blue;
		break;
	case 'c':							/* Clear / Reset */
		clearArrays();
		break;
	case 'd':					/* diff */
		if(diff == 0)
			diff = 0.001f;
		else
			diff = 0.0f;
		cout << "diff is now " << diff << "." << endl;
		break;
	case 's':					/* visc */
		if(visc == 0)
			visc = 0.001f;
		else
			visc = 0.0f;
		cout << "visc is now " << visc << "." << endl;
		break;
    default:
		break;
    }
}

// found this method of setting values on Jos Stam's website. He is awesome.
static void updateSourceMouse ( float * d, float * u, float * v )
{
	int i, j;

	// empty the (previous) arrays
	for ( i=0 ; i<size ; i++ ) 
		u[i] = v[i] = d[i] = 0.0f;

	// Check for mouse updates
	if ( !mouseState[0] && !mouseState[2] ) return;

	i = (int)((       mx /(float)windowWidth)*N+1);
	j = (int)(((windowHeight-my)/(float)windowHeight)*N+1);

	if ( i<1 || i>N || j<1 || j>N ) return;

	// left click - add velocity
	if ( mouseState[0] ) 
	{
		u[IX(i,j)] = force * (mx-omx);
		v[IX(i,j)] = force * (omy-my);
	}

	// right click - add density
	if ( mouseState[2] ) 
		d[IX(i,j)] = source;
	
	omx = mx;
	omy = my;

	return;
}


// This is the "central hub" where everything happens. Top-level function here.
void	idleLoop()
{
	updateSourceMouse( dens_prev, u_prev, v_prev);	
	// keyobard update handled in the keyboard funciton
	velStep( u, v, u_prev, v_prev, visc, dt);
	densStep( dens, dens_prev, u, v, diff, dt);
	glutPostRedisplay ();
}

int main(int argc, char* argv[])
{    
    // Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(600,600);											// set this to however big I want the window to be.
	glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH)-600)/2, (glutGet(GLUT_SCREEN_HEIGHT)-600)/2); // Centers the window.
    glutCreateWindow("SmokeRender - DelBrocco");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutMouseFunc(mouseButton);
    glutMotionFunc(mouseMotion);
    glutKeyboardFunc(keyboard);
	glutIdleFunc(idleLoop);

    // Initialize GL
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,1,0,1,-10000,10000);		// set the camera to view the bounded area for the smoke
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);

	cout << "Keyboard Inputs:" << endl;
	cout << "[Adding densities]" << endl;
	cout << "[g] - add density at the center" << endl;
	cout << "[b] - add density at the bottom center" << endl;
	cout << "[t] - add density at the top center" << endl;
	cout << "[f] - add density at the middle left" << endl;
	cout << "[v] - add density at the bottom left corner" << endl;
	cout << "[r] - add density at the top left corner" << endl;
	cout << "[h] - add density at the middle right" << endl;
	cout << "[n] - add density at the bottom right corner" << endl;
	cout << "[y] - add density at the top right corner" << endl;
	cout << "[Adding forces]" << endl;
	cout << "[i] - add force in the right upward direction at the center" << endl;
	cout << "[u] - add force in the left upward direction at the center" << endl;
	cout << "[j] - add force in the left downward direction at the center" << endl;
	cout << "[k] - add force in the right downward direction at the center" << endl;
	cout << "[p] - add force in the right upward direction from the bottom left corner" << endl;
	cout << "[o] - add force in the left upward direction from the bottom right corner" << endl;
	cout << "[l] - add force in the left downward direction from the top right corner" << endl;
	cout << "[;] - add force in the right downward direction from the top left corner" << endl;
	cout << "[a] - add force in the upward direction from the bottom center" << endl;
	cout << "[Other Commands]" << endl;
	cout << "[d] - toggle the diffusion coefficient to be zero or non-zero" << endl;
	cout << "[s] - toggle the viscocity coefficient to be zero or non-zero" << endl;
	cout << "[z] - bound/unbound densities and velocities inside the window (NOTE: also resets simulation)" << endl;
	cout << "[x] - change background color" << endl;
	cout << "[c] - Clear the screen (reset the simulation)" << endl;
	cout << "[q] - Quit the simulation" << endl << endl;

	cout << "Mouse Inputs:" << endl;
	cout << "[R-click] - add density at mouse location" << endl;
	cout << "[L-click] - add velocity at mouse location" << endl;

	// set global variables
	// "N" is already set to 64, so "size" is also already set too.
	dt = 0.1f;
	visc = 0.0f;	// animation ceases more quickly as this number increases from 0 to 1.
	diff = 0.0f;	// density rapidly dissipates as this number increases from 0 to 1. Try 0.001f for an interesting effect.
	source = 100.0f;
	force = 5.0f;
	bound = true;	// If this is true, smoke will stay on the screen. Otherwise, it can leave the screen.
	bg_blue = false;

    glutMainLoop(); // Switch to main loop
    return 0;        
}

/*

Below is the code for the fluid solver, obtained in large part from here:
http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf

Only slightly modified (and commented) by me.

*/

// sets boundaries, so the simulation can take place between walls through which smoke will not pass.
// (can experiment with these to get some interesting effects)
// the b value depends on which array we're working with:
// b = 0 for density
// b = 1 for velocity_u (horizontal)
// b = 2 for velocity_v (vertical)
void setBound(int b, float * x)
{
	if(bound)
	{
		// the edges
		for(int i = 1; i <= N; i++)
		{
			x[IX(0,i)] = b==1 ? -x[IX(1,i)] : x[IX(1,i)];		// left pixels of window
			x[IX(N+1,i)] = b==1 ? -x[IX(N,i)] : x[IX(N,i)];		// right pixles of window
			x[IX(i,0)] = b==2 ? -x[IX(i,1)] : x[IX(i,1)];		// bottom pixels of window
			x[IX(i,N+1)] = b==2 ? -x[IX(i,N)] : x[IX(i,N)];		// top pixels of window
		}

		// the four corners
		x[IX(0,0)] = 0.5 * (x[IX(1,0)] + x[IX(0,1)]);
		x[IX(0,N+1)] = 0.5 * (x[IX(1,N+1)] + x[IX(0,N)]);
		x[IX(N+1,0)] = 0.5 * (x[IX(N,0)] + x[IX(N+1,1)]);
		x[IX(N+1,N+1)] = 0.5 * (x[IX(N,N+1)] + x[IX(N+1,N)]);
	}
}

// s is the previous density array - our "source"
void addSource(float * x, float * s, float dt)
{
	for(int i = 0; i < size; i++)
		x[i] += dt*s[i];
}

// stable diffusion solver that can handle any values for diff, dt, or N: no matter how big they may be! Wowzers!
// (Gauss-Seidel relaxation)
void diffuse(int b, float * x, float * x0, float diff, float dt)
{
	int i, j, k;
	float a = dt * diff * N * N;  // remember, N is the size of one dimension of our grid - for example, 32 in a 32x32 grid

	for(k = 0; k < 20; k++) // I'm not entirely sure why this is 20, but I think it's just a number that gives enough steps for diffusion. -mvd
	{
		for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
			{
				x[IX(i,j)] = (x0[IX(i,j)] + a * (x[IX(i-1,j)] + x[IX(i+1,j)] + x[IX(i,j-1)] + x[IX(i,j+1)]))/(1+4*a);	// update the values in the density array
			}
		}
		setBound (b, x);
	}
}

// basic idea behind advection step: look for particles which end up exactly at the cell centers by tracking backwards in time from the cell centers (with a linear backtrace)
void advect(int b, float * d, float * d0, float * u, float * v, float dt)
{
	int i, j, i0, j0, i1, j1;
	float x, y, s0, t0, s1, t1, dt0;

	dt0 = dt*N;
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= N; j++)
		{
			x = i - dt0 * u[IX(i,j)];
			y = j - dt0 * v[IX(i,j)];

			// clamp so we stay in the grid
			if (x < 0.5)
				x = 0.5;
			else if (x > N + 0.5)
				x = N + 0.5;
			if (y < 0.5)
				y = 0.5;
			else if (y > N + 0.5)
				y = N + 0.5;

			// perform linear backtrace
			i0 = (int)x; i1 = i0+1;
			j0 = (int)y; j1 = j0+1;

			s1 = x - i0; s0 = 1 - s1;
			t1 = y - j0; t0 = 1 - t1;

			// linear interpolate value from 4 surrounding cells
			d[IX(i,j)] = s0 * (t0 * d0[IX(i0,j0)] + t1 * d0[IX(i0,j1)]) + s1 * (t0 * d0[IX(i1,j0)] + t1 * d0[IX(i1,j1)]);
		}
	}
	setBound(b, d);
}

// computing the height field involves the solution of some linear system called a Poisson equation - reuse Gauss-Seidel relaxation code from the density diffusion function.
void project(float * u, float * v, float * p, float * div)
{
	int i, j, k;
	float h = 1.0/N;	// grid spacing, assuming the length of each side of the grid is one

	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= N; j++)
		{
			div[IX(i,j)] = -0.5 * h * (u[IX(i+1,j)] - u[IX(i-1,j)] + v[IX(i,j+1)] - v[IX(i,j-1)]);
			p[IX(i,j)] = 0;
		}
	}
	setBound(0, div); 
	setBound(0, p);

	for(k = 0; k < 20; k++)
	{
		for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
			{
				p[IX(i,j)] = (div[IX(i,j)] + p[IX(i-1,j)] + p[IX(i+1,j)] + p[IX(i,j-1)] + p[IX(i,j+1)])/4;
			}
		}
		setBound(0, p);
	}

	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= N; j++)
		{
			u[IX(i,j)] -= 0.5 * (p[IX(i+1,j)] - p[IX(i-1,j)])/h;
			v[IX(i,j)] -= 0.5 * (p[IX(i,j+1)] - p[IX(i,j-1)])/h;
		}
	}
	setBound(1, u); setBound(2, v);
}

// force field is stored in arrays u0 and v0
void velStep(float * u, float * v, float * u0, float * v0, float visc, float dt)
{
	addSource( u, u0, dt); 
	addSource( v, v0, dt);
	SWAP(u0, u); 
	diffuse(1, u, u0, visc, dt);
	SWAP(v0, v); 
	diffuse(2, v, v0, visc, dt);
	project(u, v, u0, v0);
	SWAP(u0, u); 
	SWAP(v0, v);
	advect(1, u, u0, u0, v0, dt);
	advect(2, v, v0, u0, v0, dt);
	project(u, v, u0, v0);			// call a second time. Advect behaves more accurately when the velocity field is mass conserving.
}

// x is the density array, x0 is the previous density array, other varibles self-evident
// source densities are initially contained in the x0 array
void densStep( float * x, float * x0, float * u, float * v, float diff, float dt)
{
	addSource(x, x0, dt);
	SWAP (x0, x); diffuse (0, x, x0, diff, dt);
	SWAP (x0, x); advect (0, x, x0, u, v, dt);
} // NOTE: SWAP is a macro which simply swaps the two array pointers